<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SafetySecurityController extends Controller
{
    public function surveillanceCameras()
    {
    	return view('safety-security/surveillance-cameras');
    }

    public function accessControl()
    {
    	return view('safety-security/access-control');
    }

    public function fireAlarmSystems()
    {
    	return view('safety-security/fire-alarm-systems');
    }

    public function intruderSystems()
    {
    	return view('safety-security/intruder-systems');
    }
}
