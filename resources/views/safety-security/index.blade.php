@extends('common-layout')

@section('keywords')
	{{ "add some keywords" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - safety and security") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make it active --}}
@include('../partials/active/safety-and-security')
{{-- end make it active --}}

@section('r-promo-block')
	<div class="r-promo-block r-secondary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						We offer a comprehensive wide range of security services
					</p>
					<h1>
						Operate your business with <br> a peace of mind
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li class="active">
					Safety & Security
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/safety-and-security/surveillance-cameras">
								{{ ucwords("surveillance cameras") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/safety-and-security/access-control-time-attendance">
								{{ ucwords("access control") . " and " . ucwords("time attendance") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/safety-and-security/fire-alarm-systems">
								{{ ucwords("fire alarm systems") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/safety-and-security/intruder-systems">
								{{ ucwords("intruder systems") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')