/* navbar dropdown menu - hover effect */
$('ul.nav li.dropdown').hover (function() {
	$(this).find('.dropdown-menu').stop(true, true).fadeIn('fast');
}, function() {
	$(this).find('.dropdown-menu').stop(true, true).fadeOut('fast');
});

/* on scrolling */
function collapseNavbar() {
    var deviceWidth = document.documentElement.clientWidth;
    if ( deviceWidth > 1024) {
        if ($(".navbar").offset().top > 20) {
            $(this).css('line-height', '50px');
            $('#mainnav .container').css('padding-left', '0px');
            $('#mainnav .container').css('padding-right', '0px');
            $('#mainnav .container').css('transition', 'all .3s ease-out');
            $('#mainnav .container').css('-webkit-transition', 'all .3s ease-out');
        } else {
            $(this).css('line-height', '65px');
            $('#mainnav .container').css('padding-left', '15px');
            $('#mainnav .container').css('padding-right', '15px');
            $('#mainnav .container').css('transition', 'all .1s ease-out');
            $('#mainnav .container').css('-webkit-transition', 'all .1s ease-in');
        }
    }
}
$(window).scroll(collapseNavbar);
$(window).ready(collapseNavbar);

/* dropdown-menu customize */
$('.dropdown-menu li.dropdown').on('mouseenter', function() {
	$(this).find('.dropdown-menu').css({'display':'inline-block'}, 'slow');
}).on('mouseleave', function() {
	$(this).find('.dropdown-menu').css({'display': 'none'}, 'slow');
});

/* open URL */
function openURL(link) {
    location.href=link;
}