@extends('common-layout')

@section('keywords')
{{ "will add some keywords here" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - contact us") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						We work with industries all across Tanzania
					</p>
					<h1>
						Hire us today and move <br> your business forward
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li class="active">
					Contact Us
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We would be delighted to answer any inqury you might have about how we can help, just use the form below or choose one of the alternative methods of communication. We're available from Monday to Friday to take your call.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pt-15 r-form-block">
				<form name="callbackForm" action="" id="callbackFrom">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="name" class="text-capitalize">
							your name:
						</label>
						<input type="text" name="user-name" id="name" class="form-control" placeholder="e.g john doe">
					</div>
					<div class="form-group">
						<label for="userEmail" class="text-capitalize">
							your email: <span class="small help-block" style="display: inline;">(Optional)</span>
						</label>
						<input type="email" name="user-email" id="userEmail" class="form-control" placeholder="e.g. john@example.com">
					</div>
					<div class="form-group">
						<label for="userPhone" class="text-capitalize">
							your phone number:
						</label>
						<input type="tel" name="user-phone" id="userPhone" class="form-control" placeholder="If you want us to call you back">
					</div>
					<div class="form-group">
						<label for="subjectOfInterest" class="text-capitalize">
							subject
						</label>
						<input type="text" name="subject" id="subjectOfInterest" class="form-control" placeholder="Let us know your topic of interest">
					</div>
					<div class="form-group">
						<label for="subjectMessage" class="text-capitalize">
							your message
						</label>
						<textarea name="user-message" id="subjectMessage" rows="7" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" name="callback" class="btn btn-default btn-lg text-uppercase">
							submit
						</button>
					</div>
				</form>
			</div>
			<div class="col-lg-offset-1 col-lg-5 col-md-offset-2 col-md-4 col-sm-offset-0 col-sm-6 col-xs-12">
               <div class="r-address-block">
                   <address class="text-left">
                       <strong>Rana Technologies Limited</strong><br>
                       Sales and Support Department<br>
                       Infotech Place, 3<sup>rd</sup> Floor North<br>
                       565 Old Bagamoyo Road<br>
                       P.O.Box 79866 , Dar es Salaam, Tanzania.<br>
                       Phone Numbers:<br> 
                       <span style="padding-left: 30px;">(+255) 767 888 566</span><br>
                       <span style="padding-left: 30px;">(+255) 715 888 566</span><br>
                       Fax: (+255) 736 606 917<br>
                       Email: <a href="mailto:info@rana.co.tz">info@rana.co.tz</a><br>
                   </address>
               </div>
           </div>
		</div>
	</div>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')