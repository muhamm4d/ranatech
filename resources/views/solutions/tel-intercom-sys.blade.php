@extends('../common-layout')

@section('keywords')
{{ "will add some keywords later" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - solutions | telephone intercom systems") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make the current page active --}}
@include('../partials/active/solutions')
{{-- end make the current page active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/solutions">
						Solutions
					</a>
				</li>
				<li class="active">
					Telephone Intercom Systems
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We have a team of dedicated IT professionals with a wealth of experience within a range of IT consultancy and support services. Our experts have extensive experience in ICT Projects management.Our target is to take the hassle and expense out of customers’ IT infrastructure so that they can get on with their own business.
				</p>
			</div>
		</div>
		<div class="row pt-30">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ "VoIP Phones" }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="voice over IP phones samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("IP PBX Systems") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="pbx systems samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("public addressing (P.A) systems") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="public addressing systems samples">
				</div>
			</div>
		</div>
		<div class="row pt-30">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pb-15">
                    	<h4>{{ ucfirst("reasons to start implementing an IP PBX") }}</h4>
                	</div>
            	</div>
            	<!-- /.row title -->
            	<div class="row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    	<div class="panel-group" id="benefits">
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-1" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                        	<i class="fa fa-check-circle-o"></i>
                                        	<span>{{ "Much easier to install & configure than a proprietary phone system" }}</span>
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-1" class="panel-collapse collapse in">
                                	<div class="panel-body">
                                    	An IP PBX runs as software on a computer and can leverage the advanced processing power of the computer and user interface as well as Windows’ features. Anyone proficient in networking and computers can install and maintain an IP PBX. By contrast a proprietary phone system often requires an installer trained on that particular proprietary system!
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#1 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-2" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "Easier to manage because of web/GUI based configuration interface" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-2" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	An IP PBX can be managed via a web-based configuration interface or a GUI, allowing you to easily maintain and fine tune your phone system. Proprietary phone systems have difficult-to-use interfaces which are often designed to be used only by the phone technicians.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#2 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-3" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "Significant cost savings using VOIP providers" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-3" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	With an IP PBX you can easily use a VOIP service provider for long distance and international calls. The monthly savings are significant. If you have branch offices, you can easily connect phone systems between branches and make free phone calls.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#3 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-4" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "Eliminate phone wiring" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-4" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	An IP Telephone system allows you to connect hardware phones directly to a standard computer network port (which it can share with the adjacent computer). Software phones can be installed directly onto the PC. You can now eliminate the phone wiring and make adding or moving of extensions much easier. In new offices you can completely eliminate the extra ports to be used by the office phone system
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#4 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-5" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Eliminate vendor lock in" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-5" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				IP PBXs are based on the open SIP standard. You can now mix and match any SIP hardware or software phone with any SIP-based IP PBX, PSTN Gateway or VOIP provider. In contrast, a proprietary phone system often requires proprietary phones to use advanced features, and proprietary extension modules to add features
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#5 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-6" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Scalable" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-6" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				Proprietary systems are easy to outgrow: Adding more phone lines or extensions often requires expensive hardware modules. In some cases you need an entirely new phone system. Not so with an IP PBX: a standard computer can easily handle a large number of phone lines and extensions – just add more phones to your network to expand
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#6 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-7" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Better customer service & productivity" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-7" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				With an IP PBX you can deliver better customer service and better productivity: Since the IP telephone system is now computer-based you can integrate phone functions with business applications. For example: Bring up the customer record of the caller automatically when you receive his/her call, dramatically improving customer service and cutting cost by reducing time spent on each caller. Outbound calls can be placed directly from Outlook, removing the need for the user to type in the phone number.
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#7 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-8" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Twice the phone system features for half the price" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-8" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				Since an IP PABX is software-based, it is easier for developers to add and improve feature sets. Most VOIP phone systems come with a rich feature set, including auto attendant, voice mail, ring groups, advanced reporting and more. These options are often very expensive in proprietary systems.
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#8 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-9" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Allow hot desking & roaming" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-9" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				Hot desking – the process of being able to easily move offices/desks based on the task at hand, has become very popular. Unfortunately traditional PBXs require extensions to be re-patched to the new location. With an IP PBX the user simply takes his phone to his new desk – No patching required! <br>
                        				Users can roam too – if an employee has to work from home, he/she can simply fire up their SIP software phone and are able to answer calls to their extension, just as they would in the office. Calls can be diverted anywhere in the world because of the SIP protocol characteristics
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#9 -->
                        	<div class="panel panel-default">
                        		<div class="panel-heading">
                        			<div class="panel-title">
                        				<a href="#benefits-10" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                        					<i class="fa fa-check-circle-o"></i>
                        					{{ "Better phone usability: SIP phones are easier to use" }}
                        				</a>
                        			</div>
                        		</div>
                        		<div id="benefits-10" class="panel-collapse collapse">
                        			<div class="panel-body">
                        				Employees often struggle using advanced phone features: Setting up a conference, transferring a call – On an old PBX it all requires instruction. <br> 
                        				Not so with an IP PBX – all features are easily performed from a user friendly Windows GUI. In addition, users get a better overview of the status of other extensions and of inbound lines and call queues via the IP PBX Windows client. Proprietary systems often require expensive ‘system’ phones to get an idea what is going on your phone system. Even then, status information is cryptic at best.
                        			</div>
                        		</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#10 -->
                    	</div>
                    	<!-- /.panel-group /#benefits -->
                	</div>
            	</div>
            	<!-- /.row contents -->
        	</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')