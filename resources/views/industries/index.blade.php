@extends('common-layout')

@section('keywords')
	{{ "add some keywords here" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - industries") }}
@endsection

{{-- make it active --}}
@include('../partials/active/industries')
{{-- end make it active --}}

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li class="active">
					Industries
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="industries/banks-insurance-pension-funds">
								Banks, Insurance and Pension Funds
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="industries/corporate-offices">
								Corporate Offices
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="industries/mining-oil-gas">
								Mining, Oil & Gas
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="industries/real-estate">
								Real Estate
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="industries/telecommunications">
								Telecommunications
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')

