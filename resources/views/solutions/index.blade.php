@extends('common-layout')

@section('keywords')
{{ "Will add some keywords" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - Solutions") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make it active --}}
@include('../partials/active/solutions')
{{-- end make it active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li class="active">
					Solutions
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/solutions/audio-visual-communications">
								{{ ucwords("audio visual communications") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/solutions/data-center">
								{{ ucwords("data center") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/solutions/networking">
								{{ ucwords("networking") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/solutions/smatv">
								SMATV (IF Distribution) Systems
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" alt="">
					<div class="caption">
						<h4>
							<a href="/solutions/telephone-intercom-systems">
								{{ ucwords("telephone intercom systems") }}
							</a>
						</h4>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates, odit explicabo vel minus aspernatur similique corrupti natus, atque quos.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')
