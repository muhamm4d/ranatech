@section('r-contact-us')
	<hr>
	<div class="container">
        <div class="row r-no-padding">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 contact-msg">
                <p>Let's deliver the right solutions to your business</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 5px;">
            	<p>
            		<a href="/contact-us" class="btn btn-default btn-lg text-uppercase">
	            		contact us
	            	</a>
            	</p>
            </div>
        </div>
    </div>
@endsection