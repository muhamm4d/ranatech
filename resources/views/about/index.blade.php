@extends('common-layout')

@section('keywords')
{{ "Will add some keywords here" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - about us") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make it active --}}
@include('../partials/active/about')
{{-- end make it active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						Always expect cost effective and innovative services
					</p>
					<h1>
						Support industries with <br> superior ICT Solutions
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li class="active">
					About Us
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h3>{{ strtoupper("about us") }}</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					With headquarters in Dar es Salaam and registered in the United Republic of Tanzania Rana Technologies Ltd provides superior ICT consulting services to our clientele serving private, non-for-profit, schools colleges, government agencies, international organisations and public sector departments. Our main objective is to supply, install and support high quality cost effective ICT business solutions in the East and Central Africa region.
				</p><br>
				<span class="h3">Vision</span><br>
				<p class="ptb-15 text-italic">
					To be the leader in providing Cost effective and Innovative ICT Solutions in Tanzania
				</p>
				<span class="h3">Mission</span><br>
				<p class="ptb-15">
					<span class="text-italic">
						Our Mission is to become the company that best understands and satisfies our customers and always be Customer Centric Company.
					</span>
					As a customer-centric company we demonstrate a superior ability to understand, attract, and keep valuable customers. This ability is founded on good customer knowledge and good customer service; listening closely to customer needs and responding with solutions that meet these needs more successfully than any other company.
					Being customer-centric also means having the business agility to change and evolve along with your customers, responding to shifting needs, market change, and new opportunities as they arise. To meet this challenge, we have adaptable business culture and flexible technical infrastructure to respond to change. 
					As a company, Rana Technologies Ltd strives to fit this description in all of our activities, from our first contact with a potential customer through the full extent of our relationship we do everything to ensure will be strong and enduring and provide value to our customers. We also seek through our services and solutions to help our own customers become customer-driven market leaders.
				</p>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="commitments images">
					<div class="caption list-commitments">
						<h4>
							{{ ucwords("our commitments") }}
						</h4>
						<ul class="list-unstyled">
							<li>
								<i class="fa fa-check-circle-o"></i>
								We listen to Customers requirements, then deliver turnkey solutions
							</li>
							<li>
								<i class="fa fa-check-circle-o"></i>
								Associated with Global Reliable Brands
							</li>
							<li>
								<i class="fa fa-check-circle-o"></i>
								Excellent After Sales Support
							</li>
							<li>
								<i class="fa fa-check-circle-o"></i>
								Offering up to 5 years guarantee
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="career opportunities image">
					<div class="caption">
						<h4>
							<a href="about-us/career-opportunities">
								{{ ucwords("career opportunities") }}
							</a>
						</h4>
						<p>
							Get an opportunity to join an established ICT company and work with leaders in your field to develop innovate, experience and truly do the work that matters.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')