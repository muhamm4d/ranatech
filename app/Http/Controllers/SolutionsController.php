<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SolutionsController extends Controller
{
	public function audioVisualCommunications()
	{
		return view('solutions/audio-visual-communications');
	}

	public function dataCenter()
	{
		return view('solutions/data-center');
	}

	public function smatv()
	{
		return view('solutions/smatv');
	}

	public function safetySecurity()
	{
		return view('solutions/safety-security');
	}

	public function telIntercomSystems()
	{
		return view('solutions/tel-intercom-sys');
	}

	public function network()
	{
		return view('solutions/networking');
	}


}
