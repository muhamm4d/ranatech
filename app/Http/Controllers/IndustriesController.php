<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndustriesController extends Controller
{
    public function corporateOffices()
    {
    	return view('industries/corporate-offices');
    }

    public function telecommunications()
    {
    	return view('industries/telecommunications');
    }

    public function realEstate()
    {
    	return view('industries/real-estate');
    }

    public function miningOilGas()
    {
    	return view('industries/mining-oil-gas');
    }

    public function banksInsurancePensionFunds()
    {
    	return view('industries/bipf');
    }

}
