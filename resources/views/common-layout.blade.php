<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="Rana Technologies - Tanzania Premium ICT Services and Solutions">
    <meta name="author" content="muhammad a. mwinchande">
    <meta name="contact" content="mohammad.chandey@gmail.com/ammwinchande@gmail.com/mmwinchande@rana.co.tz">

    <title>@yield('title')</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">

	<!-- styles -->
	<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
	<link rel="stylesheet" href="/css/common-styles.css">

	{{-- per-page styling --}}
	@yield("custom-styling")

	<!-- favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="imgs/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/imgs/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/imgs/favicon/favicon-16x16.png">
    <link rel="manifest" href="/imgs/favicon/manifest.json">
    <link rel="mask-icon" href="/imgs/favicon/safari-pinned-tab.svg" color="#0571cb">
    <meta name="theme-color" content="#ffffff">

</head>
<body id="page-top">
	<div id="app"></div>
	{{-- navigation --}}
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="mainnav">
        <div class="container">
            <div class="navbar-header pull-left">
                <a href="/" class="navbar-brand">
                    <img src="/imgs/rana-new-logo-icon.png" alt="Rana Technologies Tanzania logo" class="img-responsive">
                </a>
            </div>

            <div class="navbar-header pull-right">
            	<ul class="nav pull-left" style="margin-top: 28px;">
            		<li class="text-black">
            			<i class="fa fa-search"></i>
            		</li>
            	</ul>
            	<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainavmenu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
            </div>
            <!-- /.navbar-header -->
            <div class="collapse navbar-collapse navbar-right" id="mainavmenu">
                <ul class="nav navbar-nav">
                    <li class="text-uppercase @yield('home')">
                        <a href="/">
                            home @yield('home-sr')
                        </a>
                    </li>
                    <li class="text-uppercase @yield('shop')" id="shop">
                        <a href="/shop">
                            shop @yield('shop-sr')
                        </a>
                    </li>
                    <li class="text-uppercase dropdown @yield('solutions')" id="solutions">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" onclick="openURL('/solutions');">
                            solutions @yield('solutions-sr')<i class="fa fa-caret-down"></i>
                            <ul class="dropdown-menu" role="menu" id="solutions-menu">
                                <li class="text-uppercase dropdown">
                                    <a href="/solutions/audio-visual-communications">
                                        Audio Visual &amp; communications
                                    </a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/solutions/data-center">data centre</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/solutions/networking">networking</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/solutions/smatv">smatv (if distribution) systems</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/solutions/telephone-intercom-systems">telephone intercom systems</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li class="text-uppercase dropdown @yield('industries')" id="industries">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" onclick="openURL('/industries');">
                            industries <i class="fa fa-caret-down"></i>
                            <ul class="dropdown-menu" role="menu" id="industries-menu">
                                <li class="text-uppercase">
                                    <a href="/industries/banks-insurance-pension-funds">
                                        banks, insurance, and pension funds
                                    </a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/industries/corporate-offices">corporate offices</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/industries/mining-oil-gas">mining, oil &amp; gas</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/industries/real-estate">real estate</a>
                                </li>
                                <li class="text-uppercase">
                                    <a href="/industries/telecommunications">telecommunications</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li class="text-uppercase @yield('safety-and-security')" id="safety-and-security">
                        <a href="/safety-and-security">
                            safety &amp; security  @yield('safety-and-security-sr')
                        </a>
                    </li>
                    <li class="text-uppercase @yield('about-us')" id="about-us">
                        <a href="/about-us">
                            about us  @yield('about-us-sr')
                        </a>
                    </li>
                    <li class="text-uppercase @yield('contact-us')" id="contact-us">
                        <a href="/contact-us" id="reach-btn" style="color: #056cd7; font-weight: 600; border-bottom: 1px solid #056cd7;">
                        	contract us  @yield('contact-us-sr')
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.collapse /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	{{-- /navigation --}}

	{{-- promo-block --}}
	<section class="r-promo-block">
		@yield("r-promo-block")
	</section>

	{{-- page menu --}}
	<section class="container r-sub-nav">
		@yield("r-sub-nav")
	</section>

	{{-- page contents --}}
	@yield("r-contents")

    <div class="r-partners">
        <div class="container">
            <div class="row r-block-padding">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="text-uppercase r-section-heading">our partners</h3>
                    <p>
                        No matter how complex your business questions, we have the capabilities and experience to deliver the answers you need to move forward.
                    </p>
                </div>
            </div>
            <!-- all images with links -->
            <div class="row r-block-padding">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- links and images of all partners -->
                </div>
            </div>
        </div>
    </div>
    {{-- /.r-partners --}}
    
    {{-- contact us message --}}
    @yield("r-contact-us")

	{{-- footer --}}
    @include('partials/footer')
    {{-- custom scritpts --}}
    @yield('scripts')
</body>
</html>
