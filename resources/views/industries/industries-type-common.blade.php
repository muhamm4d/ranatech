@extends('../common-layout')

@section('keywords')
{{ "will add some keywords later" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - industries |") }} @yield('current-industry')
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
	<link rel="stylesheet" href="/css/industry-styles.css">
@endsection

{{-- make the current page active --}}
@include('../partials/active/industries')
{{-- end make the current page active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/industries">
						Industries
					</a>
				</li>
				<li class="active">
					@yield('current-industry-nav')
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row pb-30">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We have a team of dedicated IT professionals with a wealth of experience within a range of IT consultancy and support services. Our experts have extensive experience in ICT Projects management.Our target is to take the hassle and expense out of customers’ IT infrastructure so that they can get on with their own business.
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-1-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-1-photo')
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-2-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-2-photo')
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-3-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-3-photo')
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-4-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-4-photo')
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-5-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-5-photo')
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 caption">
				@yield('industry-solution-6-text')
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					@yield('industry-solution-6-photo')
				</div>
			</div>
		</div>
		
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')

