@section('highlight-current-page')
{{ "active" }}
@endsection

@section('current-page-sr')
	<span class="sr-only">(current)</span>
@endsection