<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	return view('home/index');
    }

    public function shop()
    {
    	return view('shop/index');
    }

    public function solutions()
    {
    	return view('solutions/index');
    }

    public function industries()
    {
    	return view('industries/index');
    }

    public function safeSecurity()
    {
    	return view('safety-security/index');
    }

    public function aboutus()
    {
    	return view('about/index');
    }

    public function careerOpportunities()
    {
        return view('about/career');
    }

    public function contactus()
    {
    	return view('contact/index');
    }
}



