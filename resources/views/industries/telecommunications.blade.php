@extends('../industries/industries-type-common')

@section('current-industry')
{{ "telecommunications" }}
@endsection

@section('current-industry-nav')
	{{ "Telecommunications" }}
@endsection

{{-- industry solutions --}}
@section('industry-solution-1-text')
	<h4>
		<a href="/solutions/audio-visual-communications">
			{{ ucwords("audio visual communications") }}
		</a>
	</h4>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
	</p>
@endsection

@section('industry-solution-1-photo')
	<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="audio visual communications">
@endsection


@section('industry-solution-2-text')
	<h4>
		<a href="/solutions/data-center">
			{{ ucwords("data center") }}
		</a>
	</h4>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
	</p>
@endsection

@section('industry-solution-2-photo')
	<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="data center">
@endsection


@section('industry-solution-3-text')
	<h4>
		<a href="/solutions/networking">
			{{ ucwords("networking") }}
		</a>
	</h4>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
	</p>
@endsection

@section('industry-solution-3-photo')
	<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="networking">
@endsection


@section('industry-solution-4-text')
	<h4>
		<a href="/safety-and-security/surveillance-cameras">
			{{ ucwords("surveillance cameras") }}
		</a>
	</h4>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
	</p>
@endsection

@section('industry-solution-4-photo')
	<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="surveillance cameras">
@endsection
