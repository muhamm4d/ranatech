@extends('../common-layout')

@section('keywords')
{{ "will add some keywords later" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - solutions | smatv (if distribution) systems") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make the current page active --}}
@include('../partials/active/solutions')
{{-- end make the current page active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/solutions">
						Solutions
					</a>
				</li>
				<li class="active">
					SMATV (IF Distribution System)
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We are specialist in the design, installation and maintenance of Communal Television Distribution Systems. We provide a complete range of custom installation services to the residential, commercial and industrial sectors.
				</p>
			</div>
		</div>
		<div class="row pt-30">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("About SMATV (IF Distribution)") }}</h4>
				<p>
					SMATV (Satellite Master Antenna Television) or IF Distribution system as it is commonly known  is a network capable of delivering multiple satellite services in a MDU (Multi Dwelling Unit). Our SMATV system also known as Integrated Reception System (IRS) is capable of delivering digital satellite TV services, Broadband Internet services and CCTV in the same network. The system distributes digital signals from the Communal Antenna to each home in the MDU.
				</p>
				<p>
					This green concept helps to end disputes with your tenants about putting dishes all over your property as you are already providing them with a solution. The residents won’t have to install individual dishes on their balconies or on the roof-top. This helps in maintaining the appearance of the area and preserving the aesthetic look of the property. Our solutions are cost-effective, expandable and transparent to all value added satellite services
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="data center design samples">
				</div>
			</div>
		</div>
		<div class="row pt-30">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 c-key-benefits">
				<h4>{{ "SMATV systems are useful and needed in the following areas: " }}</h4>
				<ul class="list-unstyled">
                    <li>
                        <i class="fa fa-minus"></i>
                        <span>{{ ucwords("residential apartment buildings") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucwords("local authority buildings") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucfirst("domitories &amp; Camps") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucfirst("home Distribution in Villas") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucfirst("schools") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucfirst("hospitals") }}</span>
                    </li>
                    <li>
                       <i class="fa fa-minus"></i>
                       <span>{{ ucfirst("oil Platforms and Rigs") }}</span>
                    </li>
                </ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 r-block-padding">
            	<div class="row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    	<h4>{{ ucfirst("key benefits to the Landlords") }}</h4>
                	</div>
            	</div>
            	<!-- /.row title -->
            	<div class="row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    	<div class="panel-group" id="benefits">
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-1" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                        	<i class="fa fa-check-circle-o"></i>
                                        	<span>{{ "minimal dish solutions" }}</span>
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-1" class="panel-collapse collapse in">
                                	<div class="panel-body">
                                    	Helps preserve the aesthetic look of your property while providing services to all the residents in the apartment. Moreover you are not restricting the viewing choice of the residents.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#1 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-2" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "competitive advatange" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-2" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	Get a competitive edge over your competitors by providing this facility which in the short term will be a necessity. Whatever other features you provide for your buildings, TV is the appliance with which the residents spent their maximum time, hence facilities on this front will be valued and appreciated the most.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#2 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-3" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "cost savings" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-3" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	The network supports Broadband Internet as well as CCTV. Hence you can save on the costs by offering all these in one single solution… A huge saving, all these for a cost less than that of the CCTV services alone that you have promised your residents.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#3 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-4" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                    		<i class="fa fa-check-circle-o"></i>
                                        	{{ "security for residents" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-4" class="panel-collapse collapse">
                                	<div class="panel-body">
                                    	Since we undertake the maintenance and after sales support functions too, you won’t have to worry about different people walking in to your property on account of maintenance, service and so on. Rana Tech personnel will have authorized ID cards which shall be flashed while they enter your property.
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default i#4 -->
                        	<div class="panel panel-default">
                            	<div class="panel-heading">
                                	<h4 class="panel-title">
                                    	<a href="#benefits-5" data-toggle="collapse" data-parent="#benefits" class="text-capitalize">
                                        	{{ "some other benefits" }}
                                    	</a>
                                	</h4>
                            	</div>
                            	<div id="benefits-5" class="panel-collapse collapse">
                                	<div class="panel-body c-key-benefits">
                                        <ul>
                                        	<li>
                                        		{{ "Reduced aesthetic and physical damage to buildings caused by multiple dish installations and cable runs." }}
                                        	</li>
                                        	<li>
                                        		{{ "Reduced risk of disputes from tenants over dish location." }}
                                        	</li>
                                        	<li>
                                        		{{ "Reduced problems with local authorities over planning consent issues caused by multiple dish installations (An outlook for the future)." }}
                                        	</li>
                                        </ul>
                                	</div>
                            	</div>
                        	</div>
                        	<!-- /.panel /.panel-default #i -->
                    	</div>
                    	<!-- /.panel-group /#benefits -->
                	</div>
            	</div>
            	<!-- /.row contents -->
        	</div>
		</div>
		<div class="row pt-30">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h4>{{ "Our SMATV Services include: " }}</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 c-key-benefits">
				<ul class="list-unstyled">
                    <li>
                        <i class="fa fa-check-circle-o"></i>
                        <span>{{ "Designing a custom satellite distribution solution for your property" }}</span>
                    </li>
                    <li>
                       <i class="fa fa-check-circle-o"></i>
                       <span>{{ "Pre-wiring and termination of cable at each location specified at the apartment" }}</span>
                    </li>
                    <li>
                       <i class="fa fa-check-circle-o"></i>
                       <span>{{ "Maintenance of the distribution network" }}</span>
                    </li>
                    <li>
                       <i class="fa fa-check-circle-o"></i>
                       <span>{{ "Installations, Relocations, Subscription renewal services for the customers" }}</span>
                    </li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					SMATV Systems enhance building services, protect buildings from damage and unsightly installation of satellite antennas and provide building owners and landlords with a solution that meets both their needs and the needs of their residents. With the huge amount of planning that goes into any development sound advice from the start is critical.
				</p>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We work closely with Architects, Interior Designers, Property Developers, Building/Construction Companies & Building Owners to ensure throughout the installation, from the initial design stage to the final commissioning of the network, a high standard of work is maintained. Our highly trained and experience installation and network support teams are on-hand to offer their full support throughout the installation and thereafter.
				</p>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')