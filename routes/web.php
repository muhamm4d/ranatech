<?php
/* Practise routes */
Route::get('practise', function() {
	return view('learn/practise');
});

/* ranatech routes */
// home controllers
Route::get('/', 'HomeController@index');
Route::get('/shop', 'HomeController@shop');
Route::get('/solutions', 'HomeController@solutions');
Route::get('/industries', 'HomeController@industries');
Route::get('/safety-and-security', 'HomeController@safeSecurity');
Route::get('/about-us', 'HomeController@aboutus');
Route::get('/about-us/career-opportunities', 'HomeController@careerOpportunities');
Route::get('/contact-us', 'HomeController@contactus');
/* Pages Controllers */
// Solutions Pages Controllers
Route::get('solutions/audio-visual-communications', 'SolutionsController@audioVisualCommunications');
Route::get('solutions/data-center', 'SolutionsController@dataCenter');
Route::get('solutions/smatv', 'SolutionsController@smatv');
Route::get('solutions/telephone-intercom-systems', 'SolutionsController@telIntercomSystems');
Route::get('solutions/networking', 'SolutionsController@network');

// Industries Pages Controllers
Route::get('industries/corporate-offices','IndustriesController@corporateOffices');
Route::get('industries/telecommunications', 'IndustriesController@telecommunications');
Route::get('industries/real-estate', 'IndustriesController@realEstate');
Route::get('industries/mining-oil-gas', 'IndustriesController@miningOilGas');
Route::get('industries/banks-insurance-pension-funds', 'IndustriesController@banksInsurancePensionFunds');

// Safety and Security Pages Controllers
Route::get('safety-and-security/surveillance-cameras', 'SafetySecurityController@surveillanceCameras');
Route::get('safety-and-security/access-control-time-attendance', 'SafetySecurityController@accessControl');
Route::get('safety-and-security/fire-alarm-systems', 'SafetySecurityController@fireAlarmSystems');
Route::get('safety-and-security/intruder-systems', 'SafetySecurityController@intruderSystems');