@extends('common-layout')

@section('keywords')
	{{ "Will add some keywords" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - about us | career opportunities") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make it active --}}
@include('../partials/active/about')
{{-- end make it active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						We offer a huge opportunity for growth and development
					</p>
					<h1>
						If you are striving to be the <br> best we want you
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/about-us">
						About Us
					</a>
				</li>
				<li class="active">
					Career Opportunities
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h5>{{ strtoupper("job opportunities") }}</h5>
				<h3>{{ strtoupper("career opportunities") }}</h3>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="career opportunities image">
					<div class="caption">
						<h4 class="text-center font-primary-color">
							{{ ucwords("Design Expert") }}
						</h4>
						<p class="text-center">
							You will be working with our team of thinkers, designers and engineers to deliver compelling solutions to big name clients.
						</p>
						<p class="small text-muted text-center">
							Closing date: 16 Sept 2017
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="career opportunities image">
					<div class="caption">
						<h4 class="text-center font-secondary-color">
							{{ ucwords("product adviser") }}
						</h4>
						<p class="text-center">
							Have you dreamt of being part of a team who design, build and deliver innovative solutions to big name clients?
						</p>
						<p class="small text-muted text-center">
							Closing date: 16 Sept 2017
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="career opportunities image">
					<div class="caption">
						<h4 class="text-center text-black">
							{{ ucwords("senior data consultant") }}
						</h4>
						<p class="text-center">
							This role is critical in supporting and positioning Success as the leading provider of client solutions in the analytics market.
						</p>
						<p class="small text-muted text-center">
							Closing date: 16 Sept 2017
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')