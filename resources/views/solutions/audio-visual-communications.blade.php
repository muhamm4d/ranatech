@extends('../common-layout')

@section('keywords')
{{ "will add some keywords later" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - solutions | audio visual communications") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make the current page active --}}
@include('../partials/active/solutions')
{{-- end make the current page active --}}

@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/solutions">
						Solutions
					</a>
				</li>
				<li class="active">
					Audio Visual Communications
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row pb-30">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We have a team of dedicated IT professionals with a wealth of experience within a range of IT consultancy and support services. Our experts have extensive experience in ICT Projects management.Our target is to take the hassle and expense out of customers’ IT infrastructure so that they can get on with their own business.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("video conferencing systems") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="video conferencing systems samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("public addressing systems") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="public addressing systems samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("discussion systems") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="discussion systems samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("projectors and screens") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="projectors and screens samples">
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')