    <footer class="footer">
        <div class="footer-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                        <h3 class="text-uppercase font-primary-color">
                            <a href="/">
                                Rana Technologies Limited &reg;
                            </a>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-sub-nav">
                        <h4 class="text-capitalize">
                            <a href="/solutions">
                                solutions
                            </a>
                        </h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="/solutions/audio-visual-communications">
                                    Audio Visual Communications
                                </a>
                            </li>
                            <li>
                                <a href="/solutions/data-center">
                                    Data Center
                                </a>
                            </li>
                            <li>
                                <a href="/solutions/smatv">
                                    SMATV (IF Distribution Systems)
                                </a>
                            </li>
                            <li>
                                <a href="/solutions/telephone-intercom-systems">
                                    Telephone Intercom Systems
                                </a>
                            </li>
                            <li>
                                <a href="/solutions/networking">
                                    Networking
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-sub-nav">
                        <h4 class="text-capitalize">
                            <a href="/industries">
                                industries
                            </a>
                        </h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="/industries/corporate-offices">
                                    Corporate Offices
                                </a>
                            </li>
                            <li>
                                <a href="/industries/telecommunications">
                                    Telecommunications
                                </a>
                            </li>
                            <li>
                                <a href="/industries/real-estate">
                                    Real Estate
                                </a>
                            </li>
                            <li>
                                <a href="/industries/mining-oil-gas">
                                    Mining, Oil &amp; Gas
                                </a>
                            </li>
                            <li>
                                <a href="/industries/banks-insuarance-pension-funds">
                                    Banks, Insurance, and Pension Funds
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-sub-nav">
                        <h4 class="text-capitalize">
                            <a href="/safety-and-security">
                                safety &amp; security 
                            </a>
                        </h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="/safety-and-security#surveillance-cameras">
                                    Surveillance Cameras
                                </a>
                            </li>
                            <li>
                                <a href="/safety-and-security#access-control">
                                    Access Control and Time Attendance
                                </a>
                            </li>
                            <li>
                                <a href="/safety-and-security#fire-alarm-systems">
                                    Fire Alarm Systems
                                </a>
                            </li>
                            <li>
                                <a href="/safety-and-security#intruder-systems">
                                    Intruder Systems
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer-sub-nav">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="text-capitalize">
                                    <a href="/shop">
                                        shop
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="text-capitalize">
                                    <a href="/about-us">
                                        about rana
                                    </a>
                                </h4>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/about-us#what-we-are-about">
                                            What We Are About 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/about-us#our-team">
                                            Our Team
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/about-us/career-opportunities">
                                            Career Opportunities
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>  
        <div class="container-fluid r-white-background">
            <div class="row-fluid">
                <div class="col-xs-12">
                    <ul class="list-inline text-uppercase">
                        <li><i class="fa fa-copyright"></i> {{ date("Y") }} Rana Technologies Limited &reg;</li>
                        <li class="pull-right">site by <a href="https://ammwinchande.terminalab.com" target="_blank">m.a. mwinchande</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    {{-- interactivity --}}
	<script src="{{ mix('/js/app.js') }}"></script>
	<script src="/js/common-scripts.js"></script>