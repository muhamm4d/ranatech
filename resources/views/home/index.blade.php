@extends('common-layout')

@section('keywords')
{{ "IP Consultancy, ICT consultancy, IT consultancy, Tanzania, Dar es Salaam, LAN, IP PBX, IP Phone, Cisco switches, VMware, Linux support, Windows support, cisco routers, web hosting, web design, IT Project Management, IP cameras, CCTV, IT security, Cyberoam, kaspersky licenses,security appliances, IT biometric, door lock biometric, call center, Data center, Data Center consultancy, Data center design, data center implementation, data center infrastructure, IP video conference, Vidyo Ip conference, asterisks based PBX, wireless hotspot, wireless backhaul, indoor wireless, outdoor wireless, IT company, Leading IT Company, ict campany, Domain Name, Domain Hosting, email hosting, email services, email solutions, postfix email server, apache web server support, sendmail support, cisco partner, cisco, system integrator in tanzania, network infrastructure, structural cabling, IT security, IP PBX, PABX, Plantonics, sophos, ICT infrastucture, IT infrastucture, Access control system, fire alarm system, fire suppression system,nurse calling system, intruder detections system, suprema, best it company in tanzania, best network company in tanzania, networking, Local area Network, Fiber network, network integrator, bosch fire alarm system" }}
@endsection

@section('title')
    {{ ucwords("rana technologies limited") }}
@endsection

@section('custom-styling')
    <link rel="stylesheet" href="/css/index-styles.css">
@endsection

{{-- make it active --}}
@section('home')
{{ "active" }}
@endsection

@section('home-sr')
    <span class="sr-only">(current)</span>
@endsection
{{-- end make it active --}}

@section('r-promo-block')
    <div class="container-fluid r-promo-block">
        <div class="carousel slide" id="r-promo-slider" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#r-promo-slider" data-slide-to="0" class="active"></li>
                <li data-target="#r-promo-slider" data-slide-to="1"></li>
                <li data-target="#r-promo-slider" data-slide-to="2"></li>
            </ol>
            <!-- sliders wrappers -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img  class="img-responsive" src="imgs/sliders/consultation-support.jpg" alt="">
                    <div class="carousel-caption">
                        <h1>
                            We help businesses<br>innovate and grow.
                        </h1>
                        <p class="text-faded">
                            No matter how complex, we have the capabilities<br>
                            and experience you need to move forward.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <img class="img-responsive" src="imgs/sliders/data-center.jpeg" alt="">
                    <div class="carousel-caption">
                        <h1>
                            We turn information<br>into actionable insights.
                        </h1>
                        <p class="text-faded">
                            We bring the breadth of our experience and<br>
                            industry knowledge to help you Succeed.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <img class="img-responsive" src="imgs/sliders/consultation-support.jpg" alt="">
                    <div class="carousel-caption">
                        <h1>
                            Our consultants deliver<br>breakthrough value.
                        </h1>
                        <p class="text-faded">
                            We inspire clients to make their most challenging<br>
                            business decisions with confidence.
                        </p>
                    </div>
                </div>
            </div>
            <!-- silder controllers -->
            <a href="#r-promo-slider" class="left carousel-control" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a href="#r-promo-slider" class="right carousel-control" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection

@section('r-contents')
    <div class="container r-summary-block">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- summary -->
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                        <div class="thumbnail thumbnail-top-border-primary">
                            <div class="caption r-block-c-title">
                                <h4 class="font-primary-color">
                                    <a href="about-us"> About Us </a>
                                </h4>
                            </div>
                            <img src="/imgs/placeholders/1920x1080.png" alt="about us" class="img-responsive">
                            <div class="caption">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                                </p>
                            </div>
                            <div class="r-block-c-footer">
                                <a href="#" class="btn btn-link text-capitalize" role="button">
                                    Discover <i class="fa fa-caret-right" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- /.r-block-c-footer -->
                        </div>
                        <!-- /.thmbnail -->
                    </div>
                    <!-- /.col-lg-4 /.col-md-4 /.col-sm-4 /.col-xs-12 -->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                        <div class="thumbnail thumbnail-top-border-secondary">
                            <div class="caption r-block-c-title">
                                <h4>
                                    <a href="solutions"> Solutions </a>
                                </h4>
                            </div>
                            <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                            <div class="caption">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                                </p>
                            </div>
                            <div class="r-block-c-footer">
                                <a href="#" class="btn btn-link text-capitalize" role="button">
                                    Discover <i class="fa fa-caret-right" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- /.r-block-c-footer -->
                        </div>
                        <!-- /.thmbnail -->
                    </div>
                    <!-- /.col-lg-4 /.col-md-4 /.col-sm-4 /.col-xs-12 -->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                        <div class="thumbnail thumbnail-top-border-black">
                            <div class="caption">
                                <h4>
                                    <a href="industries">Industries</a>
                                </h4>
                            </div>
                            <img src="/imgs/placeholders/1920x1080.png" alt="Industries we are dealing with" class="img-responsive">
                            <div class="caption">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                                </p>
                            </div>
                            <div class="r-block-c-footer">
                                <a href="#" class="btn btn-link text-capitalize" role="button">
                                    Discover <i class="fa fa-caret-right" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- /.r-block-c-footer -->
                        </div>
                        <!-- /.thmbnail -->
                    </div>
                    <!-- /.col-lg-4 /.col-md-4 /.col-sm-4 /.col-xs-12 -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.container /.r-summary-block -->

    <div class="r-solutions-block">
        <div class="container-fluid">
            <div class="row r-no-padding r-primary-background">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center text-white s-title">
                    <p class="text-white">Let's deliver the right solutions to your business</p>
                </div>
            </div>
            <!-- /.row block-title -->
        </div>
        <!-- /.container-fluid -->

        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                                <a href="solutions"> Solution Title </a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                                <a href="solutions"> Solution Title </a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                                <a href="solutions"> Solution Title </a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                                <a href="solutions"> Solution Title </a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                                <a href="solutions"> Solution Title </a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  r-block-padding">
                    <div class="thumbnail thumbnail-top-border-primary">
                        <div class="caption r-block-c-title">
                            <h4>
                            <a href="solutions"> Solution Title</a>
                            </h4>
                        </div>
                        <img src="/imgs/placeholders/1920x1080.png" alt="sample solutions we provide" class="img-responsive">
                        <div class="caption">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum aperiam similique numquam, modi id, harum rem deserunt placeat eum architecto sit.
                            </p>
                        </div>
                        <div class="r-block-c-footer">
                            <a href="#" class="btn btn-link text-capitalize" role="button">
                                Learn More <i class="fa fa-caret-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.r-block-c-footer -->
                    </div><!-- /.thmbnail -->
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.r-solutions-block -->

    <div class="r-from-us">
        <div class="container">
            <div class="row r-no-padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 r-block-padding c-key-benefits">
                    <h3>Key Benefits</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit loremips.
                    </p>
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-check-circle-o"></i>
                            <span>We help you keep your customer data secure and available 24/7</span>
                        </li>
                        <li>
                            <i class="fa fa-check-circle-o"></i>
                            We help break down barriers to innovation within your firm
                        </li>
                        <li>
                            <i class="fa fa-check-circle-o"></i>
                            We help break down barriers to innovation within your firm
                        </li>
                        <li>
                            <i class="fa fa-check-circle-o"></i>
                            We help align your brand strategy with key objectives
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 r-block-padding">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Specialized Industries</h3>
                        </div>
                    </div>
                    <!-- /.row title -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel-group" id="c-industry-type">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#c-industry-type-1" data-toggle="collapse" data-parent="#c-industry-type" class="text-capitalize">
                                                Corporate Offices
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="c-industry-type-1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe perspiciatis minus a, recusandae, officia repellendus. Molestiae rerum impedit, unde a.
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel /.panel-default i#1 -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#c-industry-type-2" data-toggle="collapse" data-parent="#c-industry-type" class="text-capitalize">
                                                telecommunications
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="c-industry-type-2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni dolorum aperiam necessitatibus culpa amet facilis voluptatem, nostrum consequuntur cumque consectetur!
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel /.panel-default i#2 -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#c-industry-type-3" data-toggle="collapse" data-parent="#c-industry-type" class="text-capitalize">
                                                real estate
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="c-industry-type-3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum enim aperiam commodi earum et non eaque eum quisquam numquam reprehenderit.
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel /.panel-default i#3 -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#c-industry-type-4" data-toggle="collapse" data-parent="#c-industry-type" class="text-capitalize">
                                                mining, oil &amp; gas
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="c-industry-type-4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium mollitia illo nobis, deserunt libero in? Repellendus laborum nesciunt provident tempora.
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel /.panel-default i#4 -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#c-industry-type-5" data-toggle="collapse" data-parent="#c-industry-type" class="text-capitalize">
                                                banks, insurance &amp; pension funds
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="c-industry-type-5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique eum odit velit ratione dolorum expedita, in. Consequatur, cupiditate, doloribus?
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel /.panel-default #i -->
                            </div>
                            <!-- /.panel-group /#c-industry-type -->
                        </div>
                    </div>
                    <!-- /.row contents -->
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.r-from-us -->

    <div class="r-testimonials">
        <div class="container">
           <div class="row r-block-padding">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                   <h3 class="text-uppercase r-section-heading">testimonials</h3>
                   <hr>
                   <p class="r-sub-title">
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste consequuntur officiis vero consequatur debitis. Voluptas necessitatibus cum animi, veniam.
                   </p>
               </div>
           </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 r-block-padding">
                    <div class="r-testimonial-img">
                        <img src="imgs/placeholders/1920x1080.png" alt="" class="img-responsive">
                    </div>
                    <div class="r-testimonial-content">
                        <blockquote>
                            <p>
                                Rana Technologies is professional, friendly, efficient, pay attention to detail and are always there to assist me. I have full confidence in Success and have recommended them on numerous occasions.
                            </p>
                            <footer> Muhammad Mwinchande &#44; <cite title="Source Title"> Terminalab</cite></footer>
                        </blockquote>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 r-block-padding">
                    <div class="r-testimonial-img">
                        <img src="imgs/placeholders/1920x1080.png" alt="" class="img-responsive">
                    </div>
                    <div class="r-testimonial-content">
                        <blockquote>
                            <p>
                                Rana Technologies is professional, friendly, efficient, pay attention to detail and are always there to assist me. I have full confidence in Success and have recommended them on numerous occasions.
                            </p>
                            <footer> Muhammad Mwinchande &#44; <cite title="Source Title"> Terminalab</cite></footer>
                        </blockquote>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 r-block-padding">
                    <div class="r-testimonial-img">
                        <img src="imgs/placeholders/1920x1080.png" alt="" class="img-responsive">
                    </div>
                    <div class="r-testimonial-content">
                        <blockquote>
                            <p>
                                Rana Technologies is professional, friendly, efficient, pay attention to detail and are always there to assist me. I have full confidence in Success and have recommended them on numerous occasions.
                            </p>
                            <footer> Muhammad Mwinchande &#44; <cite title="Source Title"> Terminalab</cite></footer>
                        </blockquote>
                    </div>
                </div>
            </div>
            <!-- /.row contents -->
        </div>
    </div>
    <!-- /.r-testimonals -->
@endsection

@section('r-contact-us')
    <div class="r-contact-us r-primary-background">
        <div class="container">
            <div class="row r-block-padding">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h3 class="text-uppercase r-section-heading">contact us</h3>
                   <hr class="r-hr-contact-us">
                   <p class="r-sub-title">
                       We would be delighted to answer any inquiry you might have about how we can help, just use the form below or choose one of the alternative methods of communication. We’re available from Monday to Friday to take your call.
                   </p>
                </div>
            </div>
            <div class="row r-block-padding">
                <!-- contact info -->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 r-form-block">
                    <form name="callbackForm" id="callbackForm" action="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="text-capitalize">
                                your name:
                            </label>
                            <input type="text" name="user-name" id="name" class="form-control" placeholder="e.g. john doe">
                        </div>
                        <div class="form-group">
                            <label for="userEmail" class="text-capitalize">
                                Your Email: <span class="small help-block" style="display: inline;">(Optional)</span>
                            </label>
                            <input type="email" name="user-email" id="userEmail" class="form-control" placeholder="e.g. john@example.com">
                        </div>
                        <div class="form-group">
                            <label for="userPhone" class="text-capitalize">
                                your phone number:
                            </label>
                            <input type="tel" name="user-phone" id="userPhone" class="form-control" placeholder="If you want us to call you back">
                        </div>
                        <div class="form-group">
                            <label for="subjectOfInterest" class="text-capitalize">
                                subject
                            </label>
                            <input type="text" name="subject" id="subjectOfInterest" class="form-control" placeholder="Let us know your topic of interest">
                        </div>
                        <div class="form-group">
                            <label for="subjecMessage" class="text-capitalize">
                                your message
                            </label>
                            <textarea name="user-message" id="subjectMessage" rows="7" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="callback" class="btn btn-lg btn-default text-uppercase">
                                submit
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-offset-1 col-lg-5 col-md-offset-2 col-md-4 col-sm-offset-0 col-sm-6 col-xs-12">
                    <div class="r-address-block">
                        <address class="text-left">
                            <strong>Rana Technologies Limited</strong><br>
                            Sales and Support Department<br>
                            Infotech Place, 3<sup>rd</sup> Floor North<br>
                            565 Old Bagamoyo Road<br>
                            P.O.Box 79866 , Dar es Salaam, Tanzania.<br>
                            Phone Number:<br> 
                            <span style="padding-left: 30px;">(+255) 767 888 566</span><br>
                            <span style="padding-left: 30px;">(+255) 715 888 566</span><br>
                            Fax: (+255) 736 606 917<br>
                            Email: <a href="mailto:info@rana.co.tz">info@rana.co.tz</a><br>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection