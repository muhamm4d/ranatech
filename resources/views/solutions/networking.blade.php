@extends('../common-layout')

@section('keywords')
{{ "will add some keywords later" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - solutions | networking") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make the current page active --}}
@include('../partials/active/solutions')
{{-- end make the current page active --}}


@section('r-promo-block')
	<div class="r-promo-block r-primary-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<p class="text-faded">
						With passion built upon creative thinking
					</p>
					<h1>
						The superior provider of <br> client ICT services
					</h1>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('r-sub-nav')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ol class="breadcrumb">
				<li>
					<a href="/">
						Home
					</a>
				</li>
				<li>
					<a href="/solutions">
						Solutions
					</a>
				</li>
				<li class="active">
					Networking
				</li>
			</ol>
		</div>
	</div>
@endsection

@section('r-contents')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>
					We have a team of dedicated IT professionals with a wealth of experience within a range of IT consultancy and support services. Our experts have extensive experience in ICT Projects management.Our target is to take the hassle and expense out of customers’ IT infrastructure so that they can get on with their own business.
				</p>
			</div>
		</div>
		<div class="row pt-30">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("structured network cabling") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="structured network cabling samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("fiber network design and deployment") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="fiber network design and deployment samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ ucwords("wireless network") }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="wireless network samples">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h4>{{ "Network Switches and Routers" }}</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam esse alias omnis obcaecati, eligendi, provident soluta enim nisi eaque ducimus, quaerat! Eum alias enim tempore quas sunt voluptatem quos saepe incidunt itaque natus, in, officiis. Voluptatum ullam natus, vero rem incidunt quaerat, quod numquam commodi accusamus dolorem libero voluptatibus veniam?
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="thumbnail r-no-padding">
					<img src="/imgs/placeholders/1920x1080.png" class="img-responsive" alt="network switches and routers samples">
				</div>
			</div>
		</div>
	</div>
	<hr>
@endsection

{{-- r-contact-us --}}
@include('../partials/contents-bottom-contact')