@extends('common-layout')

@section('keywords')
	{{ "Will add some keywords" }}
@endsection

@section('title')
	{{ ucwords("rana technologies limited - shop") }}
@endsection

@section('custom-styling')
	<link rel="stylesheet" href="/css/content-styles.css">
@endsection

{{-- make it active --}}
@section('shop')
{{ "active" }}
@endsection

@section('shop-sr')
    <span class="sr-only">(current)</span>
@endsection
{{-- end make it active --}}